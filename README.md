# 插件作用 #

增强领地的保护功能，限制GT与BC模组里的部分物品与领地交互（例如GT扳手，撬棍。BC扳手等）

# 插件用法 #

塞到服务器plugin目录下，然后重启服务器

# 如何配置 #

本插件使用了一个独立的领地flag来控制。flag名：“wrench” ，如该值为“false”，则不允许拥有该flag的领地使用上述提到的物品，否则反之
在领地插件中的config文件里添加“wrench” flag，推荐分配给玩家领地该flag，同时置该值为“false”

本插件在第一次运行时会释放config文件，文件位于./plugin/LukaTownWrenchProtect/config.yml
如想获得满意的效果，请严格填写对应ID。 

同时，提供了两个boolean值来控制是否启用对应监听，分别为：Enabled_GT_Hook、Enabled_BC_Hook

# 指令 #

/lwp reload * 重新载入config文件

# 权限 #

luka.wp.reload * 给予重新读取配置文件的权限，默认只有OP拥有

# 开源 #
本插件使用GNU协议开源，请遵循开源协议。谢谢！

# 下载地址 #
[http://cdn.lukatown.cc/luka_wrenchprotect.jar](http://cdn.lukatown.cc/luka_wrenchprotect.jar)