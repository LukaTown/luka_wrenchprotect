/*******************************************************************************
 * 版权所有 (c) 2014 icefox
 *     这一程序是自由软件，你可以遵照自由软件基金会出版的GNU通用公共许可证条款来修改和重新发布这一程序。或者用许可证的第二版，或者（根据你的选择）用任何更新的版本。
 *     发布这一程序的目的是希望它有用，但没有任何担保。甚至没有适合特定目的的隐含的担保。更详细的情况请参阅GNU通用公共许可证。
 *     你应该已经和程序一起收到一份GNU通用公共许可证的副本。如果还没有,请查看:<http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2014 icefox
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cc.lukatown.icefox.wrenchprotect;

import java.util.List;

/**
 * *******************************************
 * Created by Icefox on 14-5-26.
 * Just for LukaTownMinecraftServer
 * Web: LukaTown.cc  E-Mail: icefox@LukaTown.cc
 * ********************************************
 */

public class WrenchProtectFunction {
    private static WrenchProtect plugin = WrenchProtect.plugin;

    public static boolean checkItemsID(int itemID) {
        List<Integer> interactEventList = plugin.getConfig().getIntegerList("InteractEvent");
        boolean enable_Interact_Hook = plugin.getConfig().getBoolean("Enabled_Interact_Hook");

        if (enable_Interact_Hook) {
            return interactEventList.contains(itemID);
        }

        return false;
    }

    public static boolean checkBlockID(int blockID) {
        List<Integer> blockEventList = plugin.getConfig().getIntegerList("BlockEvent");
        boolean enable_Block_Hook = plugin.getConfig().getBoolean("Enabled_Block_Hook");

        if (enable_Block_Hook) {
            return blockEventList.contains(blockID);
        }

        return false;
    }
}
