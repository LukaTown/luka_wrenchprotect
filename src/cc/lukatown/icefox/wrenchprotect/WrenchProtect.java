/*******************************************************************************
 * 版权所有 (c) 2014 icefox
 *     这一程序是自由软件，你可以遵照自由软件基金会出版的GNU通用公共许可证条款来修改和重新发布这一程序。或者用许可证的第二版，或者（根据你的选择）用任何更新的版本。
 *     发布这一程序的目的是希望它有用，但没有任何担保。甚至没有适合特定目的的隐含的担保。更详细的情况请参阅GNU通用公共许可证。
 *     你应该已经和程序一起收到一份GNU通用公共许可证的副本。如果还没有,请查看:<http://www.gnu.org/licenses/>.
 *
 *     Copyright (c) 2014 icefox
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cc.lukatown.icefox.wrenchprotect;

import com.bekvon.bukkit.residence.protection.FlagPermissions;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * *******************************************
 * Created by Icefox on 14-5-26.
 * Just for LukaTownMinecraftServer
 * Web: LukaTown.cc  E-Mail: icefox@LukaTown.cc
 * ********************************************
 */
public class WrenchProtect extends JavaPlugin {

    public static WrenchProtect plugin;
    public static String PluginPrefix = ChatColor.GOLD + "[路卡小镇领地增强] " + ChatColor.GRAY;

    public void onEnable() {
        plugin = this;
        getCommand("lwp").setExecutor(new WrenchProtectCommandExecutor());
        getServer().getPluginManager().registerEvents(new WrenchProtectListener(this), this);
        loadConfig();
        initialiseResHook();
        SendColorLog(PluginPrefix + ChatColor.GREEN + "已加载");
    }

    public void onDisable() {
        SendColorLog(PluginPrefix + ChatColor.YELLOW + "已卸载");
    }

    public static void SendColorLog(String Log) {
        ConsoleCommandSender sender = WrenchProtect.plugin.getServer().getConsoleSender();
        sender.sendMessage(Log);
    }

    public void loadConfig() {
        if (!new File(getDataFolder() + File.separator + "config.yml").exists()) {
            saveDefaultConfig();
            SendColorLog(PluginPrefix + ChatColor.YELLOW + "第一次使用,释放config文件");
        }

        try {
            reloadConfig();
            SendColorLog(PluginPrefix + ChatColor.YELLOW + "已读入config文件");
        } catch (Exception e) {
            e.printStackTrace();
            getServer().getPluginManager().disablePlugin(this);
            SendColorLog(PluginPrefix + ChatColor.RED + "读入config文件失败，本插件已禁用。请截取上面的堆栈报告，发送至 icefox@lukatown.cc，非常感谢！");
        }

    }

    private void initialiseResHook() {
        PluginManager pluginManager = getServer().getPluginManager();
        Plugin Res = pluginManager.getPlugin("Residence");

        if (Res != null) {
            if (Res.isEnabled()) {
                pluginManager.enablePlugin(Res);
                initialiseFlag();
                SendColorLog(PluginPrefix + ChatColor.GREEN + "已经找到Residence插件，并添加了对应Flag");
            }

        } else {
            SendColorLog(PluginPrefix + ChatColor.RED + "无法找到Residence插件，本插件已禁用");
            pluginManager.disablePlugin(this);
        }
    }

    private void initialiseFlag() {
        FlagPermissions.addFlag("wrench");
    }
}
